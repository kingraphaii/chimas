from django.conf import settings
from django_hosts import patterns, host

port = None
if settings.USE_CUSTOM_PORT:
    port = settings.CUSTOM_PORT

host_patterns = patterns(
    '',
    host(r'www', 'config.urls', name='www', port=port),
    host(r'admin', 'config.admin_urls', name='admin', port=port),
    host(r'members', "config.member_urls", name='members', port=port),
    host(r'providers', "config.provider_urls", name='providers', port=port),
)
