from django.urls import path, include

from .views import (
    news_index_view, article_detail_view,
)

urlpatterns = [
    path('', news_index_view, name='news_index'),
    path('<int:pk>/', article_detail_view, name='article'),
]
