from django.db import models

from chimas.core.models import UUIDModel


class Article(UUIDModel):
    headline = models.CharField(
        max_length=255,
    )
    body = models.TextField()

    class Meta:
        verbose_name = 'news article'
        verbose_name_plural = 'news articles'
        ordering = ('-created', )

    def __str__(self):
        return f'{self.headline}'
