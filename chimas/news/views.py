from django.shortcuts import render
from django.views import generic

from chimas.news.models import Article


class NewsIndexView(generic.ListView):
    template_name = 'news.html'
    model = Article
    context_object_name = 'articles'
    paginate_by = 10


news_index_view = NewsIndexView.as_view()

class ArticleDetailView(generic.DetailView):
	template_name = 'article.html'
	model = Article
	context_object_name = 'article'


article_detail_view = ArticleDetailView.as_view()