from django.db import models

from chimas.core.models import UUIDModel


class Complaint(UUIDModel):
    STATUSES = (
        ('P', "Pending"),
        ('A', "Addressed"),
    )
    complainant = models.ForeignKey(
        'members.Member', on_delete=models.SET_NULL, null=True, blank=True, related_name='complaints',
    )
    subject = models.CharField(
        max_length=255,
    )
    message = models.TextField()
    status = models.CharField(
        choices=STATUSES, max_length=1, default='P',
    )

    class Meta:
        verbose_name = 'complaint'
        verbose_name_plural = 'complaints'

    def __str__(self):
        return f'{self.subject}'


class Reply(UUIDModel):
    complaint = models.ForeignKey(
        'complaints.Complaint', on_delete=models.CASCADE, related_name='replies',
    )
    message = models.TextField()

    class Meta:
        verbose_name = 'complaint reply'
        verbose_name_plural = 'complaint replies'

    def __str__(self):
        return self.message
