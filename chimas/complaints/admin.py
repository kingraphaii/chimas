from django.contrib import admin

from .models import (
    Complaint, Reply
)


@admin.register(Complaint)
class ComplaintAdmin(admin.ModelAdmin):
    list_display = ('complainant', 'subject', 'status', 'created',)
    list_filter = ('status', 'created')


@admin.register(Reply)
class RPLAdmin(admin.ModelAdmin):
    list_display = ('complaint', 'message', 'created', 'modified',)
    list_filter = ('created',)
