from django import forms

from chimas.core.forms import BaseCrispyForm

from .models import (
    Complaint,
)


class AddComplaintForm(BaseCrispyForm, forms.ModelForm):
    class Meta:
        model = Complaint
        fields = ['subject', 'message', 'complainant']
        widgets = {
            'complainant': forms.HiddenInput(
            )
        }

    def __init__(self, *args, **kwargs):
        super(AddComplaintForm, self).__init__(*args, **kwargs)
