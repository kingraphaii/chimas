from crispy_forms.layout import Submit
from crispy_forms.helper import FormHelper


class BaseCrispyForm:

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
