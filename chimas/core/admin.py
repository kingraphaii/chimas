from django.contrib import admin

from .models import (
    MedicalCategory, City
)

# Customize Header
admin.site.site_header = "CHIMAS Administration Panel"
# Customize Title
admin.site.site_title = "CHIMAS Administration"
# Index Title
admin.site.index_title = "CHIMAS System Administration"


@admin.register(MedicalCategory)
class MDCAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)
