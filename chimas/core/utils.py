from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template


def send_mail(subject, recipient, txt, html=None, context=None):
    from_email = "CHIMAS TEAM <accounts@chimas.com>"

    plain_text = get_template(txt)
    html_template = None
    if html:
        html_template = get_template(html)

    text_content = plain_text.render(context)
    html_content = None
    if html_template:
        html_content = html_template.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [recipient])
    if html_content:
        msg.attach_alternative(html_content, "text/html")
    msg.send()
