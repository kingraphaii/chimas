import uuid
import datetime
import random
import string

from django.db import models

from model_utils.models import TimeStampedModel


def gen_member_number():
    """
    Generates a random reg number for members
    :return: str
    """

    from chimas.members.models import Member

    prefix = "CHM"

    def gen():
        # Create Reg Number
        # -------------------------------------------
        # Generate 4 random digits from 1000 to 9999
        rand_digits = random.randint(10000, 99999)
        return f"{prefix}{str(rand_digits)}"

    member_number = gen()
    try:
        while Member.objects.filter(member_number=member_number).exists():
            # Loop to run and create another member_number if the assigned
            # member_number exists already.
            member_number = gen()
    except:
        pass
    return member_number


def gen_provider_number():
    """
    Generates a random reg number for members
    :return: str
    """

    from chimas.providers.models import Provider

    prefix = "CPR"

    def gen():
        # Create Reg Number
        # -------------------------------------------
        # Generate 4 random digits from 1000 to 9999
        rand_digits = random.randint(10000, 99999)
        return f"{prefix}{str(rand_digits)}"

    provider_number = gen()
    try:
        while Provider.objects.filter(provider_number=provider_number).exists():
            # Loop to run and create another member_number if the assigned
            # member_number exists already.
            provider_number = gen()
    except:
        pass
    return provider_number


class UUIDModel(TimeStampedModel):
    uuid_id = models.UUIDField(
        default=uuid.uuid4, unique=True, editable=False
    )

    class Meta:
        abstract = True


class MedicalCategory(UUIDModel):
    """
    e.g. Dental, Optometry
    """
    name = models.CharField(
        max_length=255
    )

    description = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'medical category'
        verbose_name_plural = 'medical categories'

    def __str__(self):
        return f'{self.name}'


class City(UUIDModel):
    name = models.CharField(
        max_length=255, unique=True,
    )

    class Meta:
        verbose_name = 'city'
        verbose_name_plural = 'cities'

    def __str__(self):
        return f'{self.name}'
