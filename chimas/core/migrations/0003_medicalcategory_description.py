# Generated by Django 2.2.1 on 2019-10-16 14:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='medicalcategory',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
    ]
