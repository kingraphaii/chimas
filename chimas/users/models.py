from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    email = models.EmailField(
        unique=True,
    )

    def __str__(self):
        return self.username

    @property
    def is_member(self):
        try:
            m = self.member
            return True
        except:
            return False

    @property
    def is_provider(self):
        try:
            p = self.provider
            return True
        except:
            return False
