from django.views import View, generic
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect

from django_hosts.resolvers import reverse_lazy

from chimas.members.models import (
    Dependant, DependantClaim, DependantBenefit
)
from chimas.providers.models import (
    Provider,
)
from chimas.complaints.forms import AddComplaintForm
from chimas.complaints.models import Complaint
from chimas.core.models import City

from .mixins import LoginRequiredMixin, MemberMixin


class BaseView(LoginRequiredMixin, MemberMixin):
    pass


class DashboardView(BaseView, generic.TemplateView):
    template_name = 'members/dashboard/index.html'


dashboard_index = DashboardView.as_view()


class ClaimsIndex(BaseView, generic.ListView):
    template_name = 'members/dashboard/claims.html'
    context_object_name = 'claims'

    def get_queryset(self):
        return self.member.claims.all()


claims_index_view = ClaimsIndex.as_view()


class CancelMembershipView(BaseView, generic.View):

    def get(self, *args, **kwargs):
        member = self.member
        member.user.is_active = False
        member.user.save()
        return HttpResponseRedirect(
            reverse_lazy('members:sign_in', host='members')
        )

cancel_membership_view = CancelMembershipView.as_view()


##############################
# Dependants Views
#
##############################

class DependantsIndex(BaseView, generic.ListView):
    template_name = 'members/dashboard/dependants/index.html'
    context_object_name = 'deps'

    def get_queryset(self):
        return self.member.dependants.all()


dependants_index_view = DependantsIndex.as_view()


class DependantDetail(BaseView, generic.DetailView):
    model = Dependant
    context_object_name = 'dep'
    template_name = 'members/dashboard/dependants/detail.html'


dependant_detail_view = DependantDetail.as_view()


class DependantsClaimsView(BaseView, generic.ListView):
    template_name = 'members/dashboard/dependants/claims.html'
    context_object_name = 'claims'

    def get_queryset(self):
        return DependantClaim.objects.filter(
            dependant__member=self.member
        )


dependants_claims_view = DependantsClaimsView.as_view()


################################
# Subscription Views
#
################################

class SubscriptionView(BaseView, generic.ListView):
    template_name = 'members/dashboard/subscription.html'
    context_object_name = 'subs'

    def get_queryset(self):
        return self.member.subscriptions.all()

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        return ctx


subscription_view = SubscriptionView.as_view()


class ProvidersIndex(BaseView, generic.ListView):
    template_name = 'members/dashboard/providers.html'
    model = City
    context_object_name = 'cities'


providers_index_view = ProvidersIndex.as_view()


class CityProvidersIndex(BaseView, generic.ListView):
    template_name = 'members/dashboard/cprovs.html'
    context_object_name = 'providers'

    def dispatch(self, request, *args, **kwargs):
        self.city = get_object_or_404(City, pk=kwargs['pk'])
        return super(CityProvidersIndex, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.city.providers.all()

    def get_context_data(self, **kwargs):
        ctx = super(CityProvidersIndex, self).get_context_data(**kwargs)
        ctx.update({
            'city': self.city,
        })
        return ctx


city_providers_index_view = CityProvidersIndex.as_view()


class BenefitsIndex(BaseView, generic.ListView):
    template_name = 'members/dashboard/benefits.html'
    context_object_name = 'benefits'

    def get_queryset(self):
        sub = self.member.subscription()
        if sub:
            return sub.member_benefits.all()
        return self.member.benefits.none


benefits_index_view = BenefitsIndex.as_view()


class ComplainsIndex(BaseView, generic.ListView):
    template_name = 'members/dashboard/complains/index.html'
    context_object_name = 'complaints'

    def get_queryset(self):
        return self.member.complaints.all()


complains_index_view = ComplainsIndex.as_view()


class CreateComplainView(BaseView, generic.CreateView):
    template_name = 'members/dashboard/complains/add.html'
    form_class = AddComplaintForm

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(**self.get_form_kwargs())

    def get_initial(self):
        initial = super().get_initial()
        initial.update({
            'complainant': self.member,
        })
        return initial

    def get_success_url(self):
        return reverse_lazy(
            viewname='members:complaint_detail',
            kwargs={'pk': self.object.pk},
            host='members'
        )


create_complain_view = CreateComplainView.as_view()


class ComplainDetailView(BaseView, generic.DetailView):
    model = Complaint
    context_object_name = 'comp'
    template_name = 'members/dashboard/complains/detail.html'


complain_detail_view = ComplainDetailView.as_view()
