from django.shortcuts import render
from django.views import View, generic
from django.contrib.auth import get_user_model, views as auth_views
from django.core import signing
from django.conf import settings
from django.http import HttpResponseForbidden, Http404, HttpResponseRedirect

from django_hosts.resolvers import reverse_lazy

from chimas.members.models import Member
from chimas.core.utils import send_mail

from .forms import GetSignUpForm, SignUpForm, AuthenticationForm

User = get_user_model()


class GetSignUp(generic.FormView):
    template_name = 'members/auth/get_link.html'
    form_class = GetSignUpForm

    def form_valid(self, form):
        member = form.get_member()
        send_mail(
            "Create CHIMAS Account",
            member.email_address,
            'members/auth/reg.txt',
            html=None,
            context={'member': member}
        )
        return super(GetSignUp, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            viewname='members:gsu_done',
            host='members'
        )


get_sign_up_view = GetSignUp.as_view()


class GSUDone(generic.TemplateView):
    template_name = 'members/auth/email_sent.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # ctx.update({'email': })
        return ctx


gsu_don_view = GSUDone.as_view()


class SignUpView(generic.CreateView):
    template_name = 'members/auth/sign_up.html'
    form_class = SignUpForm

    def __init__(self):
        self.member = None
        super(SignUpView, self).__init__()

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(member=self.member, **self.get_form_kwargs())

    def dispatch(self, request, *args, **kwargs):
        try:
            value = signing.loads(kwargs.get('member'), key=settings.CHIMAS_KEY)
        except signing.BadSignature:
            return HttpResponseForbidden("Failed to verify sign up url")
        else:
            try:
                self.member = Member.objects.get(pk=value.get('pk', None))
            except Member.DoesNotExist:
                return Http404("Could not find member record!")
            else:
                if self.member.user:
                    return HttpResponseForbidden('Member has already signed up')
                return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super(SignUpView, self).get_initial()
        initial.update({
            'username': self.member.member_number,
        })
        return initial

    def form_valid(self, form):
        user = form.save()
        self.member.user = user
        self.member.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy(
            viewname='members:sign_in',
            host='members',
        )


sign_up_view = SignUpView.as_view()


class SignInView(auth_views.LoginView):
    template_name = 'members/auth/sign_in.html'
    authentication_form = AuthenticationForm

    def get_success_url(self):
        return reverse_lazy(
            viewname='members:index',
            host='members',
        )


sign_in_view = SignInView.as_view()


class LogoutView(auth_views.LogoutView):
    next_page = reverse_lazy(
        viewname='members:sign_in',
        host='members'
    )


logout_view = LogoutView.as_view()
