from django.urls import path, include
from django.core import signing

from .views import (
    get_sign_up_view, sign_up_view, gsu_don_view, sign_in_view, logout_view,
)
from .account_views import (
    dashboard_index, dependants_index_view, dependant_detail_view,
    dependants_claims_view, claims_index_view, subscription_view,
    providers_index_view, city_providers_index_view, benefits_index_view,
    complains_index_view, complain_detail_view, create_complain_view, cancel_membership_view,
)

app_name = 'members'

deps_urls = [
    path('', dependants_index_view, name='deps_index'),
    path('claims/', dependants_claims_view, name='deps_claims_index'),
    path('<int:pk>/', dependant_detail_view, name='dep_detail'),
]

claims_urls = [
    path('', claims_index_view, name='claims_index'),
]

account_urls = [
    path('', dashboard_index, name='index'),
    path('benefits/', benefits_index_view, name='benefits_index'),
    path('claims/', include(claims_urls), ),
    path('dependants/', include(deps_urls, )),
]

providers_urls = [
    path('', providers_index_view, name='providers_index'),
    path('<int:pk>/', city_providers_index_view, name='city_providers'),
]

complaint_urls = [
    path('', complains_index_view, name='complaints_index'),
    path('<int:pk>/', complain_detail_view, name='complaint_detail'),
    path('create/', create_complain_view, name='create_complaint'),
]

urlpatterns = [
    path('', include(account_urls)),
    path('sign-up/', get_sign_up_view, name='sign_up_url'),
    path('sign-up-request-sent/', gsu_don_view, name='gsu_done'),
    path('sign-up/<str:member>/', sign_up_view, name='sign_up'),
    path('sign-in/', sign_in_view, name='sign_in'),
    path('sign-out/', logout_view, name='logout'),
    path('subscription/', subscription_view, name='subscription'),
    path('providers/', include(providers_urls)),
    path('complaints/', include(complaint_urls)),
    path('cancel-membership/', cancel_membership_view, name="cancel_membership")
]
