from django import forms
from django.contrib.auth import forms as auth_forms, get_user_model

from chimas.members.models import Member
from chimas.core.forms import BaseCrispyForm

User = get_user_model()


class GetSignUpForm(BaseCrispyForm, forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput()
    )

    def clean(self):
        data = super().clean()
        email = data.get('email', None)
        if email:
            try:
                Member.objects.get(email_address=email)
            except:
                raise forms.ValidationError('Could not find member record with that email.')

    def get_member(self):
        if self.is_valid():
            try:
                return Member.objects.get(email_address=self.cleaned_data.get('email', ''))
            except:
                return None
        return None


class SignUpForm(BaseCrispyForm, auth_forms.UserCreationForm):
    class Meta(auth_forms.UserCreationForm.Meta):
        fields = ("username", "email",)
        widgets = {
            'username': forms.TextInput(
                attrs={'disabled': 'disabled'},
            )
        }
        model = User

    def __init__(self, member, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.value_from_datadict = lambda *args: member.member_number


class AuthenticationForm(BaseCrispyForm, auth_forms.AuthenticationForm):

    def __init__(self, request=None, *args, **kwargs):
        super(AuthenticationForm, self).__init__(request, *args, **kwargs)
        for field in self:
            try:
                field.field.widget.attrs['placeholder'] = field.field.label
            except Exception as e:
                print(e)

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                "This account is inactive.",
                code='inactive',
            )
        if not user.is_member:
            raise forms.ValidationError(
                "Account disallowed",
            )
