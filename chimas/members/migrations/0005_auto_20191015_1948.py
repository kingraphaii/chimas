# Generated by Django 2.2.1 on 2019-10-15 17:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0004_auto_20191015_1937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='memberbenefit',
            name='subscription',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='member_benefit', to='members.Subscription'),
        ),
        migrations.AlterUniqueTogether(
            name='memberbenefit',
            unique_together=set(),
        ),
    ]
