from django.apps import AppConfig


class MembersConfig(AppConfig):
    name = 'chimas.members'
