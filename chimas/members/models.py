import datetime

from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import F
from django.core import signing
from django.utils.translation import ugettext_lazy as _

from django_hosts.resolvers import reverse_lazy

from chimas.core.models import UUIDModel, gen_member_number

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)


def get_current_year():
    return timezone.now().year


class Member(UUIDModel):
    MARRIED = 'M'
    SINGLE = 'S'
    DIVORCED = 'D'
    WIDOWED = 'W'
    MARITAL_STATUSES = (
        (MARRIED, "Married"),
        (SINGLE, 'Single'),
        (DIVORCED, 'Divorced'),
        (WIDOWED, 'Widowed'),
    )
    STATUSES = (
        ('A', 'Active'),
        ('S', 'Suspended'),
        ('C', 'Cancelled'),
    )
    membership_status = models.CharField(
        choices=STATUSES, default='A', max_length=1,
    )
    plan = models.ForeignKey(
        'plans.Plan', on_delete=models.CASCADE, related_name='members',
    )
    member_number = models.CharField(default=gen_member_number, max_length=30, unique=True, editable=False)
    date_joined = models.DateField(
        verbose_name=_("Date joined"),
        help_text=_("Date when member signed up for plan."), auto_now_add=True,
    )
    name = models.CharField(
        max_length=255,
    )
    dob = models.DateField()
    gender = models.CharField(
        choices=GENDER_CHOICES, max_length=1,
    )
    marital_status = models.CharField(
        verbose_name=_("Marital status"), max_length=1,
        choices=MARITAL_STATUSES, default=MARRIED, help_text=_("Member's marital status.")
    )
    national_id = models.CharField(
        max_length=12,
    )
    address = models.TextField()
    phone = models.CharField(max_length=255, )
    email_address = models.EmailField(null=True, blank=True, unique=True)

    # Next Of Kin
    nok_name = models.CharField(
        max_length=255,
    )
    nok_phone = models.CharField(
        max_length=255,
    )

    allergies = models.TextField(
        null=True, blank=True,
    )

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, related_name='member'
    )

    class Meta:
        verbose_name = 'member'
        verbose_name_plural = 'members'

    def __str__(self):
        return f'{self.name} ({self.national_id})'

    @property
    def is_active(self):
        return self.membership_status == 'A'

    def get_sign_up_url(self):
        return reverse_lazy(
            viewname='members:sign_up',
            kwargs={'member': signing.dumps({'pk': self.pk}, key=settings.CHIMAS_KEY)},
            host='members',
        )

    @property
    def age(self):
        today = timezone.now().date()
        age = today.year - self.dob.year - \
              ((today.month, today.day) < (self.dob.month, self.dob.day))
        return age

    age.fget.short_description = _("Age")

    def subscription(self):
        qs = self.subscriptions.filter(
            start__lte=timezone.now().date(), end__gte=timezone.now().date()
        )
        try:
            return qs.get()
        except:
            print("Found more than one subscription")
            return None

    @property
    def is_subscribed(self):
        if self.subscription():
            return True
        return False

    @property
    def nok(self):
        return f'{self.nok_name}, {self.nok_phone}'


class Dependant(UUIDModel):
    CHILD = "C"
    SPOUSE = "S"
    OTHER = "O"
    RELATIONSHIP_CHOICES = (
        (CHILD, "Child"),
        (SPOUSE, "Spouse"),
        (OTHER, "Other"),
    )

    member = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='dependants')
    relationship = models.CharField(
        verbose_name=_("Relationship"), max_length=1,
        choices=RELATIONSHIP_CHOICES, help_text=_("Relationship to plan registrant.")
    )
    relationship_description = models.TextField(
        verbose_name=_("Relationship description"), null=True, blank=True,
        help_text=_("If relationship is indicated as other, please explain relationship to dependant here.")
    )
    name = models.CharField(
        max_length=255,
    )
    dob = models.DateField()
    gender = models.CharField(
        choices=GENDER_CHOICES, max_length=1,
    )
    national_id = models.CharField(
        max_length=12,
    )
    phone = models.CharField(
        null=True, blank=True, max_length=255,
    )
    allergies = models.TextField(
        null=True, blank=True,
    )

    class Meta:
        verbose_name = 'dependant'
        verbose_name_plural = 'dependants'

    @property
    def number(self):
        return self.id

    def __str__(self):
        return self.name

    @property
    def age(self):
        today = timezone.now().date()
        age = today.year - self.dob.year - \
              ((today.month, today.day) < (self.dob.month, self.dob.day))
        return age

    age.fget.short_description = _("Age")

    @property
    def address(self):
        return self.member.address

    def clean(self):
        if not self.pk:
            # Validate dependant count
            if self.member.plan is None:
                raise ValidationError(
                    _("%(member)s has not been registered for a plan yet."),
                    params={
                        'member': self.member.name,
                    }
                )
            else:
                plan = self.member.plan
                if self.member.dependants.count() + 1 > plan.max_dependants:
                    raise ValidationError(
                        _("You can only have %(dependants)s dependants under the %(policy)s plan."),
                        params={
                            'dependants': plan.max_dependants,
                            'policy': plan.name.title(),
                        }
                    )
            super(Dependant, self).clean()

        if self.relationship == "O" and (self.relationship_description is None or self.relationship_description == ""):
            raise ValidationError(
                {'relationship_description': _("Explain the dependant's relationship with the plan registrant."), }
            )

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)


class Subscription(UUIDModel):
    member = models.ForeignKey(
        'Member', on_delete=models.CASCADE, related_name='subscriptions'
    )
    year = models.PositiveIntegerField(
        default=get_current_year
    )
    paid_on = models.DateField()
    amount_paid = models.DecimalField(
        decimal_places=2, max_digits=19,
    )
    balance = models.DecimalField(
        decimal_places=2, max_digits=19, default=0
    )
    payment_reference = models.CharField(
        max_length=255,
    )
    start = models.DateField()
    end = models.DateField()

    class Meta:
        verbose_name = 'plan subscription'
        verbose_name_plural = 'plan subscriptions'
        unique_together = (
            'member', 'year',
        )

    def __str__(self):
        return f'{self.member.name} ({self.year})'

    def save(self, *args, **kwargs):
        self.full_clean()

        is_update = False
        if self.pk:
            is_update = True

        super().save(*args, **kwargs)

        if not is_update:  # Will only run when subscription touches db for the 1st time
            # Add member benefits for period
            plan_benefits = self.member.plan.benefits.all()

            for benefit in plan_benefits:
                self.member.benefits.create(
                    subscription=self,
                    category=benefit.category,
                    balance=benefit.value,
                )

            # Add dependant benefit for period
            for dependant in self.member.dependants.all():
                for benefit in plan_benefits:
                    dependant.benefits.create(
                        subscription=self,
                        category=benefit.category,
                        balance=benefit.value,
                    )

    @property
    def expired(self):
        return self.start < timezone.now().date() and self.end < timezone.now().date()


class MemberBenefit(UUIDModel):
    member = models.ForeignKey(
        'Member', on_delete=models.CASCADE, related_name='benefits',
    )
    subscription = models.ForeignKey(
        'Subscription', on_delete=models.CASCADE, related_name='member_benefits'
    )
    category = models.ForeignKey(
        'core.MedicalCategory', on_delete=models.CASCADE,
    )
    balance = models.DecimalField(
        max_digits=19, decimal_places=2,
    )

    class Meta:
        verbose_name = 'member benefit'
        verbose_name_plural = 'member benefits'
        unique_together = ('member', 'category', 'subscription')

    def __str__(self):
        return f'{self.category.name} ({self.balance})'


class MemberClaim(UUIDModel):
    provider = models.ForeignKey(
        'providers.Provider', on_delete=models.CASCADE, related_name="member_claims"
    )
    member = models.ForeignKey(
        'Member', on_delete=models.CASCADE, related_name='claims'
    )
    benefit = models.ForeignKey(
        'MemberBenefit', on_delete=models.CASCADE, related_name='claims',
    )
    value = models.DecimalField(
        decimal_places=2, max_digits=19,
    )
    notes = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'member claim'
        verbose_name_plural = 'member claims'
        ordering = ('-created',)

    def __str__(self):
        return f'{self.member.name}, {self.benefit.category.name}'

    def clean(self):
        if self.benefit.subscription.expired:
            raise ValidationError('Cannot make claim on expired subscription')

        if self.benefit and self.value:
            if self.pk:
                # is update
                claim = self.__class__.objects.get(pk=self.pk)
                if (claim.benefit.balance + claim.value) < self.value:
                    raise ValidationError("Amount exceeds balance")
            else:
                if self.value > self.benefit.balance:
                    raise ValidationError("Amount exceeds balance")

        if self.benefit and self.provider:
            if self.benefit.member != self.member:
                raise ValidationError('Please select only the member\s benefits')

    def save(self, *args, **kwargs):
        self.full_clean()
        is_update = False
        old = None
        if self.pk:
            old = self.__class__.objects.get(pk=self.pk)
            is_update = True
        super().save(*args, **kwargs)
        if is_update and old:
            # Add back prev. claim  value:
            old.benefit.balance = F('balance') + old.value
            old.benefit.save()

            old.benefit.refresh_from_db()  # get updated obj

            old.benefit.balance = F('balance') - self.value
            old.benefit.save()
        else:
            self.benefit.balance = F('balance') - self.value
            self.benefit.save()


class DependantBenefit(UUIDModel):
    dependant = models.ForeignKey(
        'Dependant', on_delete=models.CASCADE, related_name='benefits',
    )
    subscription = models.ForeignKey(
        'Subscription', on_delete=models.CASCADE, related_name='dependant_benefits',
    )
    category = models.ForeignKey(
        'core.MedicalCategory', on_delete=models.CASCADE,
    )
    balance = models.DecimalField(
        max_digits=19, decimal_places=2,
    )

    class Meta:
        verbose_name = 'dependant benefit'
        verbose_name_plural = 'dependant benefits'
        unique_together = ('dependant', 'category', 'subscription')

    def __str__(self):
        return f'{self.category.name} ({self.balance})'

    def clean(self):

        if self.subscription and self.dependant:
            if self.subscription.member != self.dependant.member:
                raise ValidationError("Subscription should belong to dependant's benefactor")


class DependantClaim(UUIDModel):
    provider = models.ForeignKey(
        'providers.Provider', on_delete=models.CASCADE, related_name="dependant_claims"
    )
    dependant = models.ForeignKey(
        'Dependant', on_delete=models.CASCADE, related_name='claims'
    )
    benefit = models.ForeignKey(
        'DependantBenefit', on_delete=models.CASCADE, related_name='claims',
    )
    value = models.DecimalField(
        decimal_places=2, max_digits=19,
    )
    notes = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'dependant claim'
        verbose_name_plural = 'dependant claims'
        ordering = ('-created',)

    def __str__(self):
        return f'{self.dependant.name}, {self.benefit.category.name}'

    def clean(self):
        if self.benefit.subscription.expired:
            raise ValidationError('Cannot make claim on expired subscription')

        if self.benefit and self.value:
            if self.pk:
                # is update
                claim = self.__class__.objects.get(pk=self.pk)
                if (claim.benefit.balance + claim.value) < self.value:
                    raise ValidationError("Amount exceeds balance")
            else:
                if self.value > self.benefit.balance:
                    raise ValidationError("Amount exceeds balance")

        if self.benefit and self.provider:
            if self.benefit.dependant != self.dependant:
                raise ValidationError('Please select only the dependant\s benefits')

    def save(self, *args, **kwargs):
        self.full_clean()
        is_update = False
        old = None
        if self.pk:
            old = self.__class__.objects.get(pk=self.pk)
            is_update = True
        super().save(*args, **kwargs)
        if is_update and old:
            # Add back prev. claim  value:
            old.benefit.balance = F('balance') + old.value
            old.benefit.save()

            old.benefit.refresh_from_db()  # get updated obj

            old.benefit.balance = F('balance') - self.value
            old.benefit.save()
        else:
            self.benefit.balance = F('balance') - self.value
            self.benefit.save()
