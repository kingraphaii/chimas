from django.contrib import admin

from .models import (
    Member, Dependant, MemberBenefit, DependantBenefit,
    MemberClaim, DependantClaim, Subscription,
)


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'member_number', 'age', 'plan', 'is_subscribed', 'gender', 'dob',)
    list_filter = ('gender', 'plan__name',)
    search_fields = ('name', 'member_number',)



@admin.register(Dependant)
class DependantAdmin(admin.ModelAdmin):
    list_display = ('name', 'member', 'age', 'relationship', 'dob', 'gender',)
    list_filter = ('gender',)


@admin.register(MemberBenefit)
class MBAdmin(admin.ModelAdmin):
    list_display = ('member', 'subscription', 'category', 'balance',)
    list_filter = ('category',)


@admin.register(DependantBenefit)
class DPBAdmin(admin.ModelAdmin):
    list_display = ('dependant', 'subscription', 'category', 'balance',)


@admin.register(MemberClaim)
class MBCAdmin(admin.ModelAdmin):
    list_display = ('provider', 'member', 'benefit', 'value',)
    list_filter = ('provider', 'benefit',)


@admin.register(DependantClaim)
class DPCAdmin(admin.ModelAdmin):
    list_display = ('provider', 'dependant', 'benefit', 'value',)
    list_filter = ('provider', 'benefit',)


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('member', 'year', 'amount_paid', 'balance', 'paid_on', 'start', 'end',)
