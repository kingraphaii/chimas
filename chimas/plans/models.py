from django.db import models

from chimas.core.models import UUIDModel


class Plan(UUIDModel):
    name = models.CharField(
        max_length=255,
    )
    price = models.DecimalField(
        decimal_places=2, max_digits=19,
    )
    max_dependants = models.PositiveIntegerField(
        default=6,
    )

    class Meta:
        verbose_name = 'plan'
        verbose_name_plural = 'plans'

    def __str__(self):
        return f'{self.name} plan'


class PlanBenefit(UUIDModel):
    plan = models.ForeignKey(
        'Plan', on_delete=models.CASCADE, related_name='benefits'
    )
    category = models.ForeignKey(
        'core.MedicalCategory', on_delete=models.CASCADE,
    )
    value = models.DecimalField(
        max_digits=19, decimal_places=2,
    )

    class Meta:
        verbose_name = 'plan benefit'
        verbose_name_plural = 'plan benefits'
        unique_together = ('plan', 'category',)

    def __str__(self):
        return f'{self.plan.name} - {self.category.name} (${self.value})'
