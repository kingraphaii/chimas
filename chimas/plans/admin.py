from django.contrib import admin

from .models import (
    Plan, PlanBenefit,
)


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'max_dependants', 'created',)


@admin.register(PlanBenefit)
class PLBAdmin(admin.ModelAdmin):
    list_display = ('plan', 'category', 'value',)
    list_filter = ('plan', 'category',)
