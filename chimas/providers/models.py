from django.db import models
from django.conf import settings
from django.core import signing

from django_hosts.resolvers import reverse_lazy

from chimas.core.models import UUIDModel, gen_provider_number


class Provider(UUIDModel):
    CATS = (
        ('PHAR', 'Pharmacy'),
        ('CLI', 'Clinic'),
        ('HOS', 'Hospital'),
        ('DOC', 'Doctor'),
    )
    category = models.CharField(
        choices=CATS, max_length=255, default='CLI',
    )
    provider_number = models.CharField(default=gen_provider_number, max_length=30, unique=True, editable=False)
    name = models.CharField(
        max_length=255,
    )
    email_address = models.EmailField(
        null=True, blank=True, unique=True,
    )
    phone = models.CharField(
        max_length=255,
    )
    city = models.ForeignKey(
        to='core.City', on_delete=models.SET_NULL, null=True, blank=True, related_name='providers'
    )
    address = models.TextField()

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True,
    )

    class Meta:
        verbose_name = 'health care service provider'
        verbose_name_plural = 'health care service providers'

    def __str__(self):
        return f'{self.name}'

    def get_sign_up_url(self):
        return reverse_lazy(
            viewname='providers:sign_up',
            kwargs={'provider': signing.dumps({'pk': self.pk}, key=settings.CHIMAS_KEY)},
            host='providers',
        )

    @property
    def cat(self):
        return self.get_category_display()
