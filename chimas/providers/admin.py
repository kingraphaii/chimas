from django.contrib import admin

from .models import (
    Provider,
)


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    list_display = ('name', 'provider_number', 'email_address', 'phone', 'city', 'address', 'created',)
    list_filter = ('created', 'city',)
    search_fields = ('name', 'provider_number', )
