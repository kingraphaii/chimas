from django.views import View, generic
from django.shortcuts import get_object_or_404

from django_hosts.resolvers import reverse_lazy

from chimas.members.models import (
    Dependant, DependantClaim, DependantBenefit, Member, MemberBenefit, MemberClaim
)
from chimas.providers.models import Provider

from .mixins import LoginRequiredMixin, ProviderMixin
from .forms import SearchMemberForm, MemberClaimForm, DependantClaimForm, UpdateForm


class BaseView(LoginRequiredMixin, ProviderMixin):
    pass


class UpdateProfile(BaseView, generic.UpdateView):
    form_class = UpdateForm
    template_name = 'providers/dashboard/update.html'

    def get_object(self, queryset=None):
        return self.provider

    def get_success_url(self):
        return reverse_lazy(
            'providers:index',
            host='providers'
        )

update_profile = UpdateProfile.as_view()


class DashboardView(BaseView, generic.TemplateView):
    template_name = 'providers/dashboard/index.html'


dashboard_index = DashboardView.as_view()


class ClaimsIndex(BaseView, generic.ListView):
    template_name = 'providers/dashboard/claims.html'
    context_object_name = 'claims'

    def get_queryset(self):
        return self.provider.member_claims.all()


claims_index_view = ClaimsIndex.as_view()


class ClaimDetails(BaseView, generic.DetailView):
    template_name = 'providers/dashboard/claims/claim.html'
    model = MemberClaim
    slug_field = 'uuid_id'
    slug_url_kwarg = 'id'
    context_object_name = 'claim'


claim_detail_view = ClaimDetails.as_view()


class DependantsClaimsView(BaseView, generic.ListView):
    template_name = 'providers/dashboard/dependants/claims.html'
    context_object_name = 'claims'

    def get_queryset(self):
        return self.provider.dependant_claims.all()


dependants_claims_view = DependantsClaimsView.as_view()


class MakeClaimIndexView(BaseView, generic.FormView):
    template_name = 'providers/dashboard/claims/make.html'
    form_class = SearchMemberForm

    def get_success_url(self):
        return reverse_lazy(
            'providers:make_claim_over',
            kwargs={'cid': self.get_form().get_member().member_number},
            host='providers'
        )


make_claim_index_view = MakeClaimIndexView.as_view()


class MakeClaimOverview(BaseView, generic.DetailView):
    template_name = 'providers/dashboard/claims/over.html'
    model = Member
    slug_url_kwarg = 'cid'
    slug_field = 'member_number'
    context_object_name = 'member'


make_claim_overview = MakeClaimOverview.as_view()


class MCOMember(BaseView, generic.ListView):
    template_name = 'providers/dashboard/claims/mcom.html'
    context_object_name = 'benefits'

    def dispatch(self, request, *args, **kwargs):
        self.member = get_object_or_404(Member, member_number=kwargs['cid'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        sub = self.member.subscription()
        if sub:
            return self.member.benefits.filter(
                subscription=sub
            )
        return self.member.benefits.none()


mcom_view = MCOMember.as_view()


class AddMemberClaim(BaseView, generic.CreateView):
    template_name = 'providers/dashboard/claims/add.html'
    form_class = MemberClaimForm

    def dispatch(self, request, *args, **kwargs):
        self.member = get_object_or_404(Member, member_number=kwargs['cid'])
        self.benefit = get_object_or_404(MemberBenefit, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        ini = super().get_initial()
        ini.update({
            'provider': self.provider,
            'member': self.member,
            'benefit': self.benefit,
        })
        return ini

    def get_success_url(self):
        return reverse_lazy(
            'providers:claim',
            kwargs={'id': self.object.uuid_id},
            host='providers',
        )

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'member': self.member,
            'benefit': self.benefit,
        })
        return ctx


add_member_claim_view = AddMemberClaim.as_view()


class MDependantsView(BaseView, generic.ListView):
    template_name = 'providers/dashboard/dpc/dlist.html'
    context_object_name = 'deps'

    def dispatch(self, request, *args, **kwargs):
        self.member = get_object_or_404(Member, pk=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.member.dependants.all()


dlist_view = MDependantsView.as_view()


class DPBLView(BaseView, generic.ListView):
    template_name = 'providers/dashboard/dpc/mcom.html'
    context_object_name = 'benefits'

    def dispatch(self, request, *args, **kwargs):
        self.member = get_object_or_404(Member, pk=kwargs['pk'])
        self.dependant = get_object_or_404(Dependant, pk=kwargs['dep'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        sub = self.member.subscription()
        if sub:
            return self.dependant.benefits.filter(
                subscription=sub
            )
        return self.dependant.benefits.none()

    def get_context_data(self, **kwargs):
        ctx = super(DPBLView, self).get_context_data(**kwargs)
        ctx.update({
            'member': self.member,
            'dependant': self.dependant,
        })
        return ctx


dpbl_view = DPBLView.as_view()


class AddDependantClaim(BaseView, generic.CreateView):
    template_name = 'providers/dashboard/dpc/add.html'
    form_class = DependantClaimForm

    def dispatch(self, request, *args, **kwargs):
        self.dependant = get_object_or_404(Dependant, pk=kwargs['dep'])
        self.benefit = get_object_or_404(DependantBenefit, pk=kwargs['ben'])
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        ini = super().get_initial()
        ini.update({
            'provider': self.provider,
            'dependant': self.dependant,
            'benefit': self.benefit,
        })
        return ini

    def get_success_url(self):
        return reverse_lazy(
            'providers:dpc',
            kwargs={'id': self.object.uuid_id},
            host='providers',
        )

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({
            'dependant': self.dependant,
            'benefit': self.benefit,
        })
        return ctx


add_dependant_claim_view = AddDependantClaim.as_view()


class DPCDetail(BaseView, generic.DetailView):
    model = DependantClaim
    slug_url_kwarg = 'id'
    slug_field = 'uuid_id'
    template_name = 'providers/dashboard/dpc/claim.html'
    context_object_name = 'claim'


dpc_detail_view = DPCDetail.as_view()


class DPCIndex(BaseView, generic.ListView):
    context_object_name = 'claims'
    template_name = 'providers/dashboard/dpc/list.html'

    def get_queryset(self):
        return self.provider.dependant_claims.all()


dpc_index_view = DPCIndex.as_view()
