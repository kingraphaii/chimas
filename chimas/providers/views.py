from django.shortcuts import render
from django.views import View, generic
from django.contrib.auth import get_user_model, views as auth_views
from django.core import signing
from django.conf import settings
from django.http import HttpResponseForbidden, Http404, HttpResponseRedirect

from django_hosts.resolvers import reverse_lazy

from chimas.providers.models import Provider
from chimas.core.utils import send_mail

from .forms import GetSignUpForm, SignUpForm, AuthenticationForm

User = get_user_model()


class GetSignUp(generic.FormView):
    template_name = 'providers/auth/get_link.html'
    form_class = GetSignUpForm

    def form_valid(self, form):
        provider = form.get_provider()
        send_mail(
            "Create CHIMAS Account",
            provider.email_address,
            'providers/auth/reg.txt',
            html=None,
            context={'provider': provider}
        )
        return super(GetSignUp, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            viewname='providers:gsu_done',
            host='providers'
        )


get_sign_up_view = GetSignUp.as_view()


class GSUDone(generic.TemplateView):
    template_name = 'providers/auth/email_sent.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # ctx.update({'email': })
        return ctx


gsu_don_view = GSUDone.as_view()


class SignUpView(generic.CreateView):
    template_name = 'providers/auth/sign_up.html'
    form_class = SignUpForm

    def __init__(self):
        self.provider = None
        super(SignUpView, self).__init__()

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(provider=self.provider, **self.get_form_kwargs())

    def dispatch(self, request, *args, **kwargs):
        try:
            value = signing.loads(kwargs.get('provider'), key=settings.CHIMAS_KEY)
        except signing.BadSignature:
            return HttpResponseForbidden("Failed to verify sign up url")
        else:
            try:
                self.provider = Provider.objects.get(pk=value.get('pk', None))
            except Provider.DoesNotExist:
                return Http404("Could not find provider record!")
            else:
                if self.provider.user:
                    return HttpResponseForbidden('Provider has already signed up')
                return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super(SignUpView, self).get_initial()
        initial.update({
            'username': self.provider.provider_number,
        })
        return initial

    def form_valid(self, form):
        user = form.save()
        self.provider.user = user
        self.provider.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy(
            viewname='providers:sign_in',
            host='providers',
        )


sign_up_view = SignUpView.as_view()


class SignInView(auth_views.LoginView):
    template_name = 'providers/auth/sign_in.html'
    authentication_form = AuthenticationForm

    def get_success_url(self):
        return reverse_lazy(
            viewname='providers:index',
            host='providers',
        )


sign_in_view = SignInView.as_view()


class LogoutView(auth_views.LogoutView):
    next_page = reverse_lazy(
        viewname='providers:sign_in',
        host='providers'
    )


logout_view = LogoutView.as_view()
