from django.urls import path, include

from .views import (
    get_sign_up_view, sign_up_view, gsu_don_view, sign_in_view, logout_view,
)
from .account_views import (
    dashboard_index, claims_index_view, dependants_claims_view,
    make_claim_index_view, make_claim_overview, mcom_view, add_member_claim_view,
    claim_detail_view, dlist_view, add_dependant_claim_view, dpbl_view, dpc_detail_view, dpc_index_view,
    update_profile,
)

app_name = 'providers'

claims_urls = [
    path('', claims_index_view, name='claims_index'),
    path('<uuid:id>/', claim_detail_view, name='claim'),

    path('dpc/', dpc_index_view, name='dpc_index'),
    path('dpc/<uuid:id>/', dpc_detail_view, name='dpc'),

    path('make/', make_claim_index_view, name='make_claim_index'),
    path('make/<str:cid>/', make_claim_overview, name='make_claim_over'),

    path('make/<str:cid>/confirm', mcom_view, name='mmc'),
    path('make/<str:cid>/confirm/<int:pk>/', add_member_claim_view, name='add_member_claim'),

    # dependants
    path('make/<int:pk>/dependants/', dlist_view, name='dlist'),
    path('make/<int:pk>/dependants/<int:dep>/confirm', dpbl_view, name='dpbl'),
    path('make/<int:pk>/dependants/<int:dep>/confirm/<int:ben>/', add_dependant_claim_view, name='add_dpc'),
]

account_urls = [
    path('', dashboard_index, name='index'),
    path('claims/', include(claims_urls), ),
]
urlpatterns = [
    path('', include(account_urls)),
    path('sign-up/', get_sign_up_view, name='sign_up_url'),
    path('sign-up-request-sent/', gsu_don_view, name='gsu_done'),
    path('sign-up/<str:provider>/', sign_up_view, name='sign_up'),
    path('sign-in/', sign_in_view, name='sign_in'),
    path('sign-out/', logout_view, name='logout'),
    path('update-profile/', update_profile, name='update_profile'),
]
