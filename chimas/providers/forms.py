from django import forms
from django.contrib.auth import forms as auth_forms, get_user_model

from chimas.providers.models import Provider
from chimas.members.models import Member, MemberClaim, MemberBenefit, Dependant, DependantBenefit, DependantClaim
from chimas.core.forms import BaseCrispyForm

User = get_user_model()


class GetSignUpForm(BaseCrispyForm, forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput()
    )

    def clean(self):
        data = super().clean()
        email = data.get('email', None)
        if email:
            try:
                Provider.objects.get(email_address=email)
            except:
                raise forms.ValidationError('Could not find provider record with that email.')

    def get_provider(self):
        if self.is_valid():
            try:
                return Provider.objects.get(email_address=self.cleaned_data.get('email', ''))
            except:
                return None
        return None


class SignUpForm(BaseCrispyForm, auth_forms.UserCreationForm):
    class Meta(auth_forms.UserCreationForm.Meta):
        fields = ("username", "email",)
        widgets = {
            'username': forms.TextInput(
                attrs={'disabled': 'disabled'},
            )
        }
        model = User

    def __init__(self, provider, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.value_from_datadict = lambda *args: provider.provider_number


class AuthenticationForm(BaseCrispyForm, auth_forms.AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs):
        super(AuthenticationForm, self).__init__(request, *args, **kwargs)
        for field in self:
            try:
                field.field.widget.attrs['placeholder'] = field.field.label
            except Exception as e:
                print(e)

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                "This account is inactive.",
                code='inactive',
            )
        if not user.is_provider:
            raise forms.ValidationError(
                "Account disallowed",
            )


class SearchMemberForm(forms.Form):
    member_number = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Enter member number here', 'class': 'form-control search-form-field'},
        )
    )

    def clean(self):
        data = super().clean()
        member_id = data.get('member_number', None)
        if member_id:
            try:
                Member.objects.get(member_number=member_id)
            except:
                raise forms.ValidationError('Could not find member record with that email.')

    def get_member(self):
        if self.is_valid():
            try:
                return Member.objects.get(member_number=self.cleaned_data.get('member_number', ''))
            except:
                return None
        return None


class MemberClaimForm(BaseCrispyForm, forms.ModelForm):
    class Meta:
        model = MemberClaim
        fields = [
            'provider', 'member', 'benefit', 'value', 'notes',
        ]
        widgets = {
            'provider': forms.HiddenInput(),
            'member': forms.HiddenInput(),
            'benefit': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self:
            try:
                field.field.widget.attrs['placeholder'] = field.field.label
            except Exception as e:
                print(e)

    def clean(self):
        data = super().clean()
        benefit = data.get('benefit', None)
        value = data.get('value', None)
        if benefit and value:
            if value > benefit.balance:
                raise forms.ValidationError(f"Value cannot be greater than balance: ${benefit.balance}")


class DependantClaimForm(BaseCrispyForm, forms.ModelForm):
    class Meta:
        model = DependantClaim
        fields = [
            'provider', 'dependant', 'benefit', 'value', 'notes',
        ]
        widgets = {
            'provider': forms.HiddenInput(),
            'dependant': forms.HiddenInput(),
            'benefit': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self:
            try:
                field.field.widget.attrs['placeholder'] = field.field.label
            except Exception as e:
                print(e)

    def clean(self):
        data = super().clean()
        benefit = data.get('benefit', None)
        value = data.get('value', None)
        if benefit and value:
            if value > benefit.balance:
                raise forms.ValidationError(f"Value cannot be greater than balance: ${benefit.balance}")


class UpdateForm(BaseCrispyForm, forms.ModelForm):
    class Meta:
        model = Provider
        fields = [
            'address', 'phone', 'email_address',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self:
            try:
                field.field.widget.attrs['placeholder'] = field.field.label
            except Exception as e:
                print(e)
