from django.contrib.auth import get_user_model, mixins

from django_hosts.resolvers import reverse_lazy

User = get_user_model()


class LoginRequiredMixin(mixins.LoginRequiredMixin):

    def get_login_url(self):
        return reverse_lazy(
            viewname='providers:sign_in',
            host='providers'
        )

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()

        user = User.objects.get(email=request.user.email)
        if not user.is_provider:
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)


class ProviderMixin:
    def __init__(self, *args, **kwargs):
        self.provider = None
        super().__init__(*args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        user = User.objects.get(username=request.user.username)
        self.provider = user.provider
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.provider:
            ctx.update({
                'provider': self.provider
            })
        return ctx
